## Lợi ích khi sử dụng Unit of Work design pattern?
### Khái niệm UOW
+ First it maintains in-memory updates.
+ Second it sends these in-memory updates as one transaction to the database.
Giải thích:
– Thứ 1: Unit Of Work quản lý các trạng thái cập nhật dữ liệu trong bộ nhớ.

– Thứ 2: Unit Of Work thực hiện cập nhật tất cả dữ liệu trong bộ nhớ “như là – một transaction” vào database.

Từ khái niệm trên, chúng ta rút ra được lợi ích khi sử dụng Unit Of Work:
+ Quản lý danh sách các đối tượng logic nghiệp vụ được thay đổi trong một transaction.
+ Khi một transaction được complete, tất cả thay đổi sẽ được gửi tới database như một big unit of work
## Work và Unit trong ứng dụng phần mềm.
Một cách đơn giản để định nghĩa Worklà: "Thực hiện một vài tác vụ". Các tác vụ ở đây có thể là Thêm, Sửa, Xóa dữ liệu.
Chúng ta có thể cùng xem một ví dụ về việc ứng dụng quản lý khách hàng. Khi chúng ta thêm/sửa/xóa khách hàng trong database, đó là một unit. Kịch bản đơn giản của chúng ta đó là:
  ```sh
 1 customer CRUD = 1 unit of work
  ```
![alt text](http://thaotrinh.info/wp-content/uploads/2019/05/d1.jpg)

 ## Logical transaction! = Physical CRUD
Trong bài toán thực tế, một khách hàng có thể có nhiều địa chỉ giao hàng khác nhau (địa chỉ nhà, địa chỉ cơ quan…).
Tiếp theo ví dụ trên, chúng ta xem xét ông khách hàng Shiv. Giả sử ông này có 2 địa chỉ, vậy khi thay đổi thông tin của khách hàng Shiv này, chúng ta cần thay đổi 3 bản ghi. Xem xét kịch bản như sau:
  ```sh
  3 Customer CRUD = 1 Logical unit of work
  ```
![alt text](http://thaotrinh.info/wp-content/uploads/2019/05/d2.jpg)

Trong các dự án thực tế, chúng ta có thể đưa nhiều tác vụ khác nhau vào một unit of work.
Một ví dụ đơn giản khác, đó chính là bài toán rút tiền tại cây ATM. Khi khách hàng thực hiện rút tiền, ngân hàng cập nhật thông tin bảng giao dịch, cập nhật số dư khách hàng, gửi tin nhắn, in hóa đơn.. Tất cả các tác vụ đó có thể đưa vào một UOW
## So sánh với simple transaction
Một UOW sẽ rất khác với một transaciton database. UOW chỉ gửi những bản ghi được thay đổi tới database mà không phải tất cả các bản ghi!

Điều này có nghĩa là gì?
Ví dụ ứng dụng của bạn lấy từ DB ra 3 bản ghi dữ liệu, nhưng chỉ thay đổi 2 trong 3 bản ghi đó. UOW chỉ gửi 2 bản ghi được thay đổi này tới DB để tiến hành cập nhật. Việc này giúp tối ưu performance cho hệ thống của bạn.
![alt text](http://thaotrinh.info/wp-content/uploads/2019/05/d3.jpg)
Tóm lại
  ```sh
  1 Unit of work = Modified records in a transaction
  ```
![alt text](http://thaotrinh.info/wp-content/uploads/2019/05/d4.jpg)
## So sánh với mô hình thông thường
![alt text](https://media.enlabsoftware.com/wp-content/uploads/2021/01/07154300/image1.png)

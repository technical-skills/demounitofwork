﻿using System.Threading.Tasks;

namespace DemoUnitOfWork.Infrastructure.Interfaces
{
    public interface IUnitOfWork 
    {
        /// <summary>
        /// Call save change from db context.
        /// </summary>
        void SaveChanges();

        /// <summary>
        /// Call save change from db context async.
        /// </summary>
        Task SaveChangeAsync();
    }
}
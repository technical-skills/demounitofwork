﻿using DemoUnitOfWork.ApplicationDbContext;
using DemoUnitOfWork.Infrastructure.Interfaces;
using System.Threading.Tasks;

namespace DemoUnitOfWork.Infrastructure.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        DemoUOWDbContext _context;

        public UnitOfWork( DemoUOWDbContext context)
        {
            _context = context;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangeAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}

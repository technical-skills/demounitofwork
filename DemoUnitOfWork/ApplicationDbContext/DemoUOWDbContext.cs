﻿using DemoUnitOfWork.Entities;
using Microsoft.EntityFrameworkCore;

namespace DemoUnitOfWork.ApplicationDbContext
{
    public class DemoUOWDbContext : DbContext
    {
        public DemoUOWDbContext(DbContextOptions<DemoUOWDbContext> options) : base(options)
        {

        }

        public DbSet<OrderEntities> Orders { get; set; }

        public DbSet<OrderDetailEntities> OrderDetails { get; set; }

        public DbSet<ProductEntities> Products { get; set; }
    }
}

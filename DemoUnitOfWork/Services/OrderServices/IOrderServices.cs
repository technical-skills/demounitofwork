﻿using DemoUnitOfWork.Models;
using System.Collections.Generic;

namespace DemoUnitOfWork.Services.OrderServices
{
    public interface IOrderServices
    {
        List<OrderViewModel> GetAllOrder();

        void CreateOrder(OrderViewModel viewModel);

        OrderViewModel UpdateOrder(OrderViewModel viewModel);

        bool DeleleOrder(int orderId);

        void SaveChanges();
    }
}

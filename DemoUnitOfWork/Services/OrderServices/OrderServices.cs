﻿using DemoUnitOfWork.Entities;
using DemoUnitOfWork.Infrastructure.Interfaces;
using DemoUnitOfWork.Models;
using DemoUnitOfWork.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DemoUnitOfWork.Services.OrderServices
{
    public class OrderServices : IOrderServices
    {
        private readonly IOrderRepository _orderRepository;

        private readonly IOrderDetailRepository _orderDetailRepository;

        private readonly IProductRepository _productRepository;

        private readonly IUnitOfWork _unitOfWork;

        public OrderServices(
            IOrderRepository orderRepository, 
            IOrderDetailRepository orderDetailRepository, 
            IProductRepository productRepository, 
            IUnitOfWork unitOfWork)
        {
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
            _productRepository = productRepository;
            _unitOfWork = unitOfWork;
        }

        public void CreateOrder(OrderViewModel viewModel)
        {
            List<OrderDetailEntities> OrderDetails = new List<OrderDetailEntities>();
            OrderEntities orderInsert = new OrderEntities()
            {
                CustomerName = viewModel.CustomerName,
                CustomerAddress = viewModel.CustomerAddress
            };

            foreach (var detail in viewModel.OrderDetails)
            {
                OrderDetailEntities orderDetail = new OrderDetailEntities();
                orderDetail.ProductId = detail.ProductId;
                orderDetail.Quantity = detail.Quantity;

                var priceProduct = _productRepository.FindById(detail.ProductId);
                orderDetail.Price = priceProduct.Price;

                OrderDetails.Add(orderDetail);
            }
            orderInsert.OrderDetails = OrderDetails;
            _orderRepository.Add(orderInsert);
        }

        public bool DeleleOrder(int orderId)
        {
            
            var order = _orderRepository.FindAll(x => x.Id == orderId).SingleOrDefault();
            var orderDetail = _orderDetailRepository.FindAll(x => x.OrderId == orderId).ToList();
            if (orderDetail == null || order == null)
                return false;
            else
            {
                _orderDetailRepository.RemoveMultiple(orderDetail);
                _orderRepository.Remove(order);
                return true;
            }    
        }

        public List<OrderViewModel> GetAllOrder()
        {
            throw new NotImplementedException();
        }

        public OrderViewModel UpdateOrder(OrderViewModel viewModel)
        {
            throw new NotImplementedException();
        }

        public void SaveChanges()
        {
            _unitOfWork.SaveChanges();
        }
    }
}

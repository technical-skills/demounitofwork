﻿using DemoUnitOfWork.Entities;
using DemoUnitOfWork.Infrastructure.Interfaces;
using DemoUnitOfWork.Models;
using DemoUnitOfWork.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace DemoUnitOfWork.Services.ProductServices
{
    public class ProductServices : IProductServices
    {
        private readonly IProductRepository _productRepository;

        private readonly IUnitOfWork _unitOfWork;

        public ProductServices(
            IProductRepository productRepository, 
            IUnitOfWork unitOfWork)
        {
            _productRepository = productRepository;
            _unitOfWork = unitOfWork;
        }

        public bool DeleteProduct(int productId)
        {
            var productExist = _productRepository.FindById(productId);
            if (productExist == null)
                return false;
            _productRepository.Remove(productExist);
            return true;
        }

        public ProductViewModel InsertProduct(ProductViewModel dataInput)
        {
            ProductEntities productInsert = new ProductEntities()
            {
                Name = dataInput.Name,
                Description = dataInput.Description,
                Category = dataInput.Category,
                Price = dataInput.Price
            };
            _productRepository.Add(productInsert);
            return dataInput;
        }

        public List<ProductViewModel> ListProduct()
        {
            var product = _productRepository.FindAll().Select(x => new ProductViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Category = x.Category,
                Description = x.Description,
                Price = x.Price
            }).ToList();
            return product;
        }

        public void SaveChange()
        {
            _unitOfWork.SaveChanges();
        }

        public ProductViewModel UpdateProduct(int productId, ProductViewModel dataInput)
        {
            var productExist = _productRepository.FindById(productId);
            if (productExist == null)
                return null;
            else
            { 
                productExist.Id = dataInput.Id;
                productExist.Name = dataInput.Name;
                productExist.Description = dataInput.Description;
                productExist.Category = dataInput.Category;
                productExist.Price = dataInput.Price;
                _productRepository.Update(productExist);
                return dataInput;
            }
        }
    }
}

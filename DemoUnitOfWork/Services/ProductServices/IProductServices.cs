﻿using DemoUnitOfWork.Models;
using System.Collections.Generic;

namespace DemoUnitOfWork.Services.ProductServices
{
    public interface IProductServices
    {
        List<ProductViewModel> ListProduct();

        ProductViewModel InsertProduct(ProductViewModel dataInput);

        ProductViewModel UpdateProduct(int productId,ProductViewModel dataInput);

        bool DeleteProduct(int productId);

        void SaveChange();
    }
}

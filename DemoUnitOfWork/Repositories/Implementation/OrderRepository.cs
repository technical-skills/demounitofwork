﻿using DemoUnitOfWork.ApplicationDbContext;
using DemoUnitOfWork.Entities;
using DemoUnitOfWork.Infrastructure.Implementation;
using DemoUnitOfWork.Repositories.Interfaces;

namespace DemoUnitOfWork.Repositories.Implementation
{
    public class OrderRepository : Repository<OrderEntities, int>, IOrderRepository
    {
        public OrderRepository(DemoUOWDbContext context) : base(context)
        {
        }
    }
}

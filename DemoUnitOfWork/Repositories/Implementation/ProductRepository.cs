﻿using DemoUnitOfWork.ApplicationDbContext;
using DemoUnitOfWork.Entities;
using DemoUnitOfWork.Infrastructure.Implementation;
using DemoUnitOfWork.Repositories.Interfaces;

namespace DemoUnitOfWork.Repositories.Implementation
{
    public class ProductRepository : Repository<ProductEntities, int>, IProductRepository
    {
        public ProductRepository(DemoUOWDbContext context): base(context)
        {
        }
    }
}

﻿using DemoUnitOfWork.ApplicationDbContext;
using DemoUnitOfWork.Entities;
using DemoUnitOfWork.Infrastructure.Implementation;
using DemoUnitOfWork.Repositories.Interfaces;

namespace DemoUnitOfWork.Repositories.Implementation
{
    public class OrderDetailRepository : Repository<OrderDetailEntities, int>, IOrderDetailRepository
    {
        public OrderDetailRepository(DemoUOWDbContext context) : base(context)
        {
        }
    }
}

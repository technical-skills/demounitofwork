﻿using DemoUnitOfWork.Entities;

namespace DemoUnitOfWork.Repositories.Interfaces
{
    public interface IOrderRepository : IRepository<OrderEntities, int>
    {
    }
}

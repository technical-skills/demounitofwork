﻿using DemoUnitOfWork.Entities;

namespace DemoUnitOfWork.Repositories.Interfaces
{
    public interface IOrderDetailRepository : IRepository<OrderDetailEntities, int>
    {
    }
}

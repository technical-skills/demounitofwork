﻿using DemoUnitOfWork.Entities;

namespace DemoUnitOfWork.Repositories.Interfaces
{
    public interface IProductRepository : IRepository<ProductEntities, int>
    {
    }
}

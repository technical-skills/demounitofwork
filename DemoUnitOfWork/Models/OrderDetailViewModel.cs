﻿namespace DemoUnitOfWork.Models
{
    public class OrderDetailViewModel
    {
        public int Id { get; set; }

        /// <summary>
        /// Id đơn đặt hàng.
        /// </summary>
        public int OrderId { set; get; }

        /// <summary>
        /// Id sản phẩm.
        /// </summary>
        public int ProductId { set; get; }

        /// <summary>
        /// Số lượng.
        /// </summary>
        public int Quantity { set; get; }

        /// <summary>
        /// Giá.
        /// </summary>
        public decimal Price { set; get; }
    }
}

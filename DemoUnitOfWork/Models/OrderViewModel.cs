﻿using System.Collections.Generic;

namespace DemoUnitOfWork.Models
{
    public class OrderViewModel
    {
        public int Id { get; set; }

        public string CustomerName { set; get; }

        public string CustomerAddress { set; get; }

        public List<OrderDetailViewModel> OrderDetails { set; get; }
    }
}

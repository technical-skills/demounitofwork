﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DemoUnitOfWork.Entities
{
    public class ProductEntities : BaseEntity<int>
    {
        [StringLength(255)]
        [Required]
        public string Name { get; set; }

        [Required]
        public string Category { get; set; }

        [Required]
        [DefaultValue(0)]
        public decimal Price { get; set; }

        [StringLength(255)]
        public string Description { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DemoUnitOfWork.Entities
{
    public class OrderDetailEntities : BaseEntity<int>
    {
        /// <summary>
        /// Id sản phẩm.
        /// </summary>
        public int ProductId { set; get; }

        /// <summary>
        /// Số lượng.
        /// </summary>
        public int Quantity { set; get; }

        /// <summary>
        /// Giá.
        /// </summary>
        public decimal Price { set; get; }

        /// <summary>
        /// Id đơn đặt hàng.
        /// </summary>
        public int OrderId { set; get; }

        /// <summary>
        /// Thuộc tính điều hướng tới bảng OrderEntities.
        /// </summary>
        [ForeignKey("OrderId")]
        public virtual OrderEntities Order { set; get; }
    }
}

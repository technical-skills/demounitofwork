﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DemoUnitOfWork.Entities
{
    public class OrderEntities : BaseEntity<int>
    {
        [Required]
        [MaxLength(256)]
        public string CustomerName { set; get; }

        [Required]
        [MaxLength(256)]
        public string CustomerAddress { set; get; }

        public List<OrderDetailEntities> OrderDetails { set; get; }
    }
}

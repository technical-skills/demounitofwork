﻿using DemoUnitOfWork.Models;
using DemoUnitOfWork.Services.OrderServices;
using Microsoft.AspNetCore.Mvc;

namespace DemoUnitOfWork.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class OrderController : Controller
    {
        private IOrderServices _orderServices;

        public OrderController(IOrderServices orderServices)
        {
            _orderServices = orderServices;
        }

        [HttpPost("CreateOrder")]
        public OrderViewModel CreateOrder([FromBody] OrderViewModel orderViewModel)
        {
            _orderServices.CreateOrder(orderViewModel);
            _orderServices.SaveChanges();
            return orderViewModel;
        }
        
        [HttpPost("DeleteOrder")]
        public string DeleteOrder(int orderId)
        {
            var resultDelete = _orderServices.DeleleOrder(orderId);
            if (!resultDelete)
                return "Không tồn tại đơn hàng này";
            else
            {
                _orderServices.SaveChanges();
                return "Xóa đơn hàng thành công";
            } 
        }
    }
}

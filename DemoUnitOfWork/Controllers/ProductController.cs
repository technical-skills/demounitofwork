﻿using DemoUnitOfWork.Models;
using DemoUnitOfWork.Services.ProductServices;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace DemoUnitOfWork.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class ProductController : Controller
    {
        private readonly IProductServices _productServices;

        public ProductController(IProductServices productServices)
        {
            _productServices = productServices;
        }

        [HttpPost("CreateProduct")]
        public ProductViewModel CreateProduct([FromBody] ProductViewModel productViewModel)
        {
            _productServices.InsertProduct(productViewModel);
            _productServices.SaveChange();
            return productViewModel;
        }

        [HttpPost("UpdateProduct")]
        public ProductViewModel UpdateProduct([FromBody] ProductViewModel productViewModel, int productId)
        {
            _productServices.UpdateProduct(productId,productViewModel);
            _productServices.SaveChange();
            return productViewModel;
        }
        
        [HttpDelete("DeleteProduct")]
        public string DeleteProduct(int productId)
        {
            if (_productServices.DeleteProduct(productId))
            {
                _productServices.SaveChange();
                return "Xóa thành công";
            }    
            return "Sản phẩm này không tồn tại";
        }
        [HttpGet("GetProduct")]
        public List<ProductViewModel> GetProduct()
        {
            return _productServices.ListProduct();
        }
    }
}
